import pickle
import numpy as np
import matplotlib.pyplot as plt
n=0.05
plt.figure()
with open('database0.3.pickle', 'rb') as f:
    entry=pickle.load(f)

entryfire=entry[0][1]['totalfiredtree']
entrytotal=entry[0][1]['totaltree']
draw=[]
drawtotal=[]
for n in range(50):
	draw.append(entryfire[n])
	drawtotal.append(entrytotal[n])
draw=np.asarray(draw,dtype=np.double)
drawtotal=np.asarray(drawtotal,dtype=np.double)
plt.plot(draw/drawtotal,label='protective firing')

with open('datebase5.pickle', 'rb') as f:
    entry=pickle.load(f)
print entry
entryfire=entry['totalfiredtree']
entrytotal=entry['totaltree']
draw=[]
drawtotal=[]

for n in range(50):
	draw.append(entryfire[n])
	drawtotal.append(entrytotal[n])
draw=np.asarray(draw,dtype=np.double)
drawtotal=np.asarray(drawtotal,dtype=np.double)
plt.plot(draw/drawtotal,label='no protective firing')
plt.xlabel('time')
plt.ylabel('Total burned trees')
plt.legend()
plt.show()
