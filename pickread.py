import pickle
import numpy as np
import matplotlib.pyplot as plt
n=0.05

while (n<=0.5):
    with open('database'+str(n)+'.pickle', 'rb') as f:
        entry=pickle.load(f)
        draw=np.asarray(entry[0][1]['totalfiredtree'])
        plt.plot(draw)
    
    n=n+0.1
plt.show()
