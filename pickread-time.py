import pickle
import numpy as np
import matplotlib.pyplot as plt
n=0.05

while (n<=0.5):
    with open('database'+str(n)+'.pickle', 'rb') as f:
        entry=pickle.load(f)
        draw=np.asarray(entry[0][1]['totalfiredtree'],dtype=np.double)
        drawtotal=np.asarray(entry[0][1]['totaltree'],dtype=np.double)
        plt.plot(draw/drawtotal,'o--',label='threshold '+str(n))
    
    n=n+0.1
plt.xlabel('time')
plt.ylabel('Percentage burned trees')
plt.legend()
plt.show()
