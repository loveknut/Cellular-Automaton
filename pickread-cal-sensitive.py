import pickle
import numpy as np
import matplotlib.pyplot as plt
plt.figure(figsize=(10,10))
for p in range(40,50,2):
    n=0.05
    p=p/100.0
    drawx=[]
    drawy=[]
    while (n<=0.5):
        with open('database'+str(n)+'.pickle', 'rb') as f:
            entry=pickle.load(f)
            ffire=np.asarray(entry[0][1]['ffire'])
            totalfiredtree=np.asarray(entry[0][1]['totalfiredtree'])
            total=np.asarray(entry[0][1]['totaltree'])
            score=p*np.sum(ffire)/np.sum(total)+(1-p)*np.sum(totalfiredtree)/np.sum(total)
            print score
            drawx.append(n)
            drawy.append(score)
        n=n+0.05
    drawx=np.asarray(drawx,dtype=np.double)
    drawy=np.asarray(drawy)
    plt.plot(drawx,drawy,'o--',label='p = '+str(p))


plt.xlabel('threshold')
plt.ylabel('Score')
plt.legend()
plt.savefig('figure_3.png')
plt.show()
