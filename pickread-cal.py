import pickle
import numpy as np
import matplotlib.pyplot as plt
n=0.05

while (n<=0.5):
    with open('database'+str(n)+'.pickle', 'rb') as f:
        entry=pickle.load(f)
        ffire=np.asarray(entry[0][1]['ffire'])
        totalfiredtree=np.asarray(entry[0][1]['totalfiredtree'])
        total=np.asarray(entry[0][1]['totaltree'])
        score=0.45*np.sum(ffire)/np.sum(total)+0.55*np.sum(totalfiredtree)/np.sum(total)
        print score
        plt.plot(n,score,'o')
    n=n+0.05
plt.show()
